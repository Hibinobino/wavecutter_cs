﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Media;

namespace WaveFile
{
    public struct WHD
    {
        public string R_Chunk;
        public int R_Size;
        public string R_wav;

        public string F_Chunk;
        public int F_Size;
        public short F_fmt;
        public short F_ch;
        public int F_sample;
        public int F_bps;
        public short F_block;
        public short F_bit;
        // + ex

        public string D_Chunk;
        public int D_Size;

        public int[] data;
        public byte[] bytedata;
        //public int[] data_R;
    }
    public class DealWave
    {
        //waveの情報を載せた構造体
        public WHD whd = new WHD();

        //ヘッダーのサイズ
        int whdsize;
        string[] ch = { "null", "mono", "stereo" };
        long filesize;
        SoundPlayer player;

        public bool ReadWave(string file)
        {
            using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader bin = new BinaryReader(fs))
                {
                    whd.R_Chunk = Encoding.UTF8.GetString(bin.ReadBytes(4));
                    whd.R_Size = bin.ReadInt32();
                    whd.R_wav = Encoding.UTF8.GetString(bin.ReadBytes(4));

                    whd.F_Chunk = Encoding.UTF8.GetString(bin.ReadBytes(4));
                    whd.F_Size = bin.ReadInt32();
                    whd.F_fmt = bin.ReadInt16();
                    whd.F_ch = bin.ReadInt16();
                    whd.F_sample = bin.ReadInt32();

                    if (whd.F_sample != 44100) return false;

                    whd.F_bps = bin.ReadInt32();
                    whd.F_block = bin.ReadInt16();
                    whd.F_bit = bin.ReadInt16();

                    if (whd.F_bit != 16) return false;

                    // + ex
                    if (whd.F_Size > 16) bin.ReadBytes(whd.F_Size - 16);
                    whdsize = 44 + (whd.F_Size - 16);

                    whd.D_Chunk = Encoding.UTF8.GetString(bin.ReadBytes(4));
                    whd.D_Size = bin.ReadInt32();

                    whd.data = new int[whd.D_Size / whd.F_block];

                    //バイトを整数配列に格納、ここでモノラルに変換
                    for (int x = 0; x < whd.D_Size / whd.F_block; x++)
                    {
                        whd.data[x] = bin.ReadInt16();
                        if (whd.F_ch == 2) whd.data[x] = whd.data[x] / 2 + bin.ReadInt16() / 2;
                    }
                }
            }
            whd.bytedata = new byte[whd.data.Length * 2];
            using (MemoryStream ms = new MemoryStream())
            {
                for (int x = 0; x < whd.data.Length; x++)
                {
                    ms.Write(BitConverter.GetBytes(whd.data[x]), 0, 2);
                }
                ms.Seek(0, SeekOrigin.Begin);
                ms.Read(whd.bytedata, 0, whd.data.Length * 2);
            }
            return true;
        }

        public void PlayWave(int from, int to)
        {
            if (player != null) player.Stop();
            to = Math.Min(to, whd.data.Length);
            to -= 1;
            //from = int.Parse(start.Text);
            //to = int.Parse(end.Text);
            int samples = to - from;

            int ch = 1;
            int bps = ch * whd.F_bit / 8 * whd.F_sample;
            int block = ch * whd.F_bit / 8;

            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(Encoding.UTF8.GetBytes(whd.R_Chunk), 0, 4);
                ms.Write(BitConverter.GetBytes(samples * block + whdsize - 8), 0, 4);
                ms.Write(Encoding.UTF8.GetBytes(whd.R_wav), 0, 4);

                ms.Write(Encoding.UTF8.GetBytes(whd.F_Chunk), 0, 4);
                ms.Write(BitConverter.GetBytes(16), 0, 4);
                ms.Write(BitConverter.GetBytes(whd.F_fmt), 0, 2);
                ms.Write(BitConverter.GetBytes(ch), 0, 2); //F_ch
                ms.Write(BitConverter.GetBytes(whd.F_sample), 0, 4);
                ms.Write(BitConverter.GetBytes(bps), 0, 4);
                ms.Write(BitConverter.GetBytes(block), 0, 2);
                ms.Write(BitConverter.GetBytes(whd.F_bit), 0, 2);

                ms.Write(Encoding.UTF8.GetBytes(whd.D_Chunk), 0, 4);
                ms.Write(BitConverter.GetBytes(samples * block), 0, 4);

                ms.Write(whd.bytedata, from * block, samples * block);

                ms.Seek(0, SeekOrigin.Begin);

                player = new SoundPlayer(ms);
                player.Play();
                Console.WriteLine(from + "～" + to + "を再生しました");
            }
        }

        public void SaveWave(int from, int to, string fullpath)
        {
            char[] invalidChars = Path.GetInvalidFileNameChars();
            string fname = Path.GetFileName(fullpath);
            if (fname.IndexOfAny(invalidChars) >= 0) return;
            if (fname == "") return;

            to = Math.Min(to, whd.data.Length);
            to -= 1;
            int samples = to - from;
            int ch = 1;
            int bps = ch * whd.F_bit / 8 * whd.F_sample;
            int block = ch * whd.F_bit / 8;
            int count = 1;
            string app = "";

            //ファイルの存在確認
            //Console.WriteLine("Check Exist...");
            while (File.Exists(fullpath + app + ".wav") == true)
            {
                app = count.ToString();
                count += 1;
            }
            //Console.WriteLine("Save As..." + (fullpath + ".wav"));
            using (FileStream ms = new FileStream(fullpath + app + ".wav", FileMode.Create, FileAccess.Write))
            {
                ms.Write(Encoding.UTF8.GetBytes(whd.R_Chunk), 0, 4);
                ms.Write(BitConverter.GetBytes(samples * block + whdsize - 8), 0, 4);
                ms.Write(Encoding.UTF8.GetBytes(whd.R_wav), 0, 4);

                ms.Write(Encoding.UTF8.GetBytes(whd.F_Chunk), 0, 4);
                ms.Write(BitConverter.GetBytes(16), 0, 4);
                ms.Write(BitConverter.GetBytes(whd.F_fmt), 0, 2);
                ms.Write(BitConverter.GetBytes(ch), 0, 2); //F_ch
                ms.Write(BitConverter.GetBytes(whd.F_sample), 0, 4);
                ms.Write(BitConverter.GetBytes(bps), 0, 4);
                ms.Write(BitConverter.GetBytes(block), 0, 2);
                ms.Write(BitConverter.GetBytes(whd.F_bit), 0, 2);

                ms.Write(Encoding.UTF8.GetBytes(whd.D_Chunk), 0, 4);
                ms.Write(BitConverter.GetBytes(samples * block), 0, 4);

                ms.Write(whd.bytedata, from * block, samples * block);
            }
            //Console.WriteLine("Saved Successfully.");
        }
    }
}