﻿namespace WavecutVS
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menu_file = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_load = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.menu_open = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_save = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_saveas = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.menu_export = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.menu_end = new System.Windows.Forms.ToolStripMenuItem();
            this.表示VToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.波形モードToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_wavemode1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_wavemode2 = new System.Windows.Forms.ToolStripMenuItem();
            this.互換データToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_oldlabel = new System.Windows.Forms.ToolStripMenuItem();
            this.なめうぇーぶ互換ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_vflread = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_vflsave = new System.Windows.Forms.ToolStripMenuItem();
            this.pic = new System.Windows.Forms.PictureBox();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.dialog_load = new System.Windows.Forms.OpenFileDialog();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.stlabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.stlabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.wavemenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.全体表示ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.選択部分を上でカットToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.選択部分を下でカットToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tool_load = new System.Windows.Forms.ToolStripButton();
            this.tool_open = new System.Windows.Forms.ToolStripButton();
            this.tool_save = new System.Windows.Forms.ToolStripButton();
            this.tool_saveas = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolzoomin = new System.Windows.Forms.ToolStripButton();
            this.toolzoomout = new System.Windows.Forms.ToolStripButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.dialog_openold = new System.Windows.Forms.OpenFileDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.dialog_opennew = new System.Windows.Forms.OpenFileDialog();
            this.dialog_savenew = new System.Windows.Forms.SaveFileDialog();
            this.dialog_savetxt = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.wavemenu.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(28, 28);
            this.menuStrip1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_file,
            this.表示VToolStripMenuItem,
            this.互換データToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(678, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menu_file
            // 
            this.menu_file.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_load,
            this.toolStripSeparator4,
            this.menu_open,
            this.menu_save,
            this.menu_saveas,
            this.toolStripSeparator2,
            this.menu_export,
            this.toolStripSeparator3,
            this.menu_end});
            this.menu_file.Name = "menu_file";
            this.menu_file.Size = new System.Drawing.Size(67, 20);
            this.menu_file.Text = "ファイル(&F)";
            // 
            // menu_load
            // 
            this.menu_load.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.menu_load.Image = ((System.Drawing.Image)(resources.GetObject("menu_load.Image")));
            this.menu_load.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.menu_load.Name = "menu_load";
            this.menu_load.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.menu_load.Size = new System.Drawing.Size(262, 22);
            this.menu_load.Text = "wavを読み込む";
            this.menu_load.Click += new System.EventHandler(this.menu_load_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(259, 6);
            // 
            // menu_open
            // 
            this.menu_open.Enabled = false;
            this.menu_open.Name = "menu_open";
            this.menu_open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.menu_open.Size = new System.Drawing.Size(262, 22);
            this.menu_open.Text = "作業データを開く";
            this.menu_open.Click += new System.EventHandler(this.menu_open_Click);
            // 
            // menu_save
            // 
            this.menu_save.Enabled = false;
            this.menu_save.Name = "menu_save";
            this.menu_save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.menu_save.Size = new System.Drawing.Size(262, 22);
            this.menu_save.Text = "作業データを上書き保存";
            this.menu_save.Click += new System.EventHandler(this.menu_save_Click);
            // 
            // menu_saveas
            // 
            this.menu_saveas.Enabled = false;
            this.menu_saveas.Name = "menu_saveas";
            this.menu_saveas.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.menu_saveas.Size = new System.Drawing.Size(262, 22);
            this.menu_saveas.Text = "作業データを別名で保存";
            this.menu_saveas.Click += new System.EventHandler(this.menu_saveas_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(259, 6);
            // 
            // menu_export
            // 
            this.menu_export.Enabled = false;
            this.menu_export.Name = "menu_export";
            this.menu_export.Size = new System.Drawing.Size(262, 22);
            this.menu_export.Text = "一括書き出し";
            this.menu_export.Click += new System.EventHandler(this.menu_export_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(259, 6);
            // 
            // menu_end
            // 
            this.menu_end.Name = "menu_end";
            this.menu_end.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.menu_end.Size = new System.Drawing.Size(262, 22);
            this.menu_end.Text = "終了";
            this.menu_end.Click += new System.EventHandler(this.menu_end_Click);
            // 
            // 表示VToolStripMenuItem
            // 
            this.表示VToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.波形モードToolStripMenuItem});
            this.表示VToolStripMenuItem.Name = "表示VToolStripMenuItem";
            this.表示VToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.表示VToolStripMenuItem.Text = "表示(&V)";
            // 
            // 波形モードToolStripMenuItem
            // 
            this.波形モードToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_wavemode1,
            this.menu_wavemode2});
            this.波形モードToolStripMenuItem.Name = "波形モードToolStripMenuItem";
            this.波形モードToolStripMenuItem.Size = new System.Drawing.Size(123, 22);
            this.波形モードToolStripMenuItem.Text = "波形モード";
            // 
            // menu_wavemode1
            // 
            this.menu_wavemode1.Checked = true;
            this.menu_wavemode1.CheckOnClick = true;
            this.menu_wavemode1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.menu_wavemode1.Name = "menu_wavemode1";
            this.menu_wavemode1.Size = new System.Drawing.Size(105, 22);
            this.menu_wavemode1.Text = "モード1";
            this.menu_wavemode1.Click += new System.EventHandler(this.menu_wavemode1_Click);
            // 
            // menu_wavemode2
            // 
            this.menu_wavemode2.CheckOnClick = true;
            this.menu_wavemode2.Name = "menu_wavemode2";
            this.menu_wavemode2.Size = new System.Drawing.Size(105, 22);
            this.menu_wavemode2.Text = "モード2";
            this.menu_wavemode2.Click += new System.EventHandler(this.menu_wavemode2_Click);
            // 
            // 互換データToolStripMenuItem
            // 
            this.互換データToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_oldlabel,
            this.なめうぇーぶ互換ToolStripMenuItem});
            this.互換データToolStripMenuItem.Name = "互換データToolStripMenuItem";
            this.互換データToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.互換データToolStripMenuItem.Text = "互換データ";
            // 
            // menu_oldlabel
            // 
            this.menu_oldlabel.Enabled = false;
            this.menu_oldlabel.Name = "menu_oldlabel";
            this.menu_oldlabel.Size = new System.Drawing.Size(176, 22);
            this.menu_oldlabel.Text = "旧版作業データを開く";
            this.menu_oldlabel.Click += new System.EventHandler(this.menu_oldlabel_Click);
            // 
            // なめうぇーぶ互換ToolStripMenuItem
            // 
            this.なめうぇーぶ互換ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_vflread,
            this.menu_vflsave});
            this.なめうぇーぶ互換ToolStripMenuItem.Name = "なめうぇーぶ互換ToolStripMenuItem";
            this.なめうぇーぶ互換ToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.なめうぇーぶ互換ToolStripMenuItem.Text = "なめうぇーぶ対応";
            // 
            // menu_vflread
            // 
            this.menu_vflread.Enabled = false;
            this.menu_vflread.Name = "menu_vflread";
            this.menu_vflread.Size = new System.Drawing.Size(201, 22);
            this.menu_vflread.Text = "VFL読み込み（まだ）";
            // 
            // menu_vflsave
            // 
            this.menu_vflsave.Enabled = false;
            this.menu_vflsave.Name = "menu_vflsave";
            this.menu_vflsave.Size = new System.Drawing.Size(201, 22);
            this.menu_vflsave.Text = "対応txt書き出し（上段）";
            this.menu_vflsave.Click += new System.EventHandler(this.menu_vflsave_Click);
            // 
            // pic
            // 
            this.pic.BackColor = System.Drawing.Color.Pink;
            this.pic.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.pic.Dock = System.Windows.Forms.DockStyle.Top;
            this.pic.Location = new System.Drawing.Point(0, 49);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(678, 200);
            this.pic.TabIndex = 1;
            this.pic.TabStop = false;
            this.pic.Click += new System.EventHandler(this.pic_Click);
            this.pic.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pic_MouseDoubleClick);
            this.pic.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pic_MouseDown);
            this.pic.MouseEnter += new System.EventHandler(this.pic_MouseEnter);
            this.pic.MouseLeave += new System.EventHandler(this.pic_MouseLeave);
            this.pic.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pic_MouseMove);
            this.pic.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pic_MouseUp);
            this.pic.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.pic_PreviewKeyDown);
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.hScrollBar1.Location = new System.Drawing.Point(0, 249);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(678, 21);
            this.hScrollBar1.TabIndex = 2;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // dialog_load
            // 
            this.dialog_load.Filter = "wavファイル|*.wav";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stlabel1,
            this.stlabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 271);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(678, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // stlabel1
            // 
            this.stlabel1.Name = "stlabel1";
            this.stlabel1.Size = new System.Drawing.Size(31, 17);
            this.stlabel1.Text = "Tips:";
            // 
            // stlabel2
            // 
            this.stlabel2.Name = "stlabel2";
            this.stlabel2.Size = new System.Drawing.Size(269, 17);
            this.stlabel2.Text = "wavを読み込むとこのへんに使い方が出るかもしれない。";
            // 
            // wavemenu
            // 
            this.wavemenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.全体表示ToolStripMenuItem,
            this.toolStripSeparator1,
            this.選択部分を上でカットToolStripMenuItem,
            this.選択部分を下でカットToolStripMenuItem});
            this.wavemenu.Name = "wavemenu";
            this.wavemenu.Size = new System.Drawing.Size(179, 76);
            // 
            // 全体表示ToolStripMenuItem
            // 
            this.全体表示ToolStripMenuItem.Name = "全体表示ToolStripMenuItem";
            this.全体表示ToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.全体表示ToolStripMenuItem.Text = "全体表示";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(175, 6);
            // 
            // 選択部分を上でカットToolStripMenuItem
            // 
            this.選択部分を上でカットToolStripMenuItem.Name = "選択部分を上でカットToolStripMenuItem";
            this.選択部分を上でカットToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.選択部分を上でカットToolStripMenuItem.Text = "選択部分を上でカット";
            // 
            // 選択部分を下でカットToolStripMenuItem
            // 
            this.選択部分を下でカットToolStripMenuItem.Name = "選択部分を下でカットToolStripMenuItem";
            this.選択部分を下でカットToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.選択部分を下でカットToolStripMenuItem.Text = "選択部分を下でカット";
            // 
            // toolStrip1
            // 
            this.toolStrip1.AllowMerge = false;
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool_load,
            this.tool_open,
            this.tool_save,
            this.tool_saveas,
            this.toolStripSeparator5,
            this.toolzoomin,
            this.toolzoomout});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(678, 25);
            this.toolStrip1.TabIndex = 9;
            // 
            // tool_load
            // 
            this.tool_load.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tool_load.Image = ((System.Drawing.Image)(resources.GetObject("tool_load.Image")));
            this.tool_load.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_load.Name = "tool_load";
            this.tool_load.Size = new System.Drawing.Size(23, 22);
            this.tool_load.Text = "wavを読み込む";
            this.tool_load.Click += new System.EventHandler(this.menu_load_Click);
            // 
            // tool_open
            // 
            this.tool_open.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tool_open.Enabled = false;
            this.tool_open.Image = ((System.Drawing.Image)(resources.GetObject("tool_open.Image")));
            this.tool_open.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_open.Name = "tool_open";
            this.tool_open.Size = new System.Drawing.Size(23, 22);
            this.tool_open.Text = "作業データを開く";
            this.tool_open.Click += new System.EventHandler(this.menu_open_Click);
            // 
            // tool_save
            // 
            this.tool_save.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tool_save.Enabled = false;
            this.tool_save.Image = ((System.Drawing.Image)(resources.GetObject("tool_save.Image")));
            this.tool_save.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_save.Name = "tool_save";
            this.tool_save.Size = new System.Drawing.Size(23, 22);
            this.tool_save.Text = "作業データを上書き保存";
            this.tool_save.Click += new System.EventHandler(this.menu_save_Click);
            // 
            // tool_saveas
            // 
            this.tool_saveas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tool_saveas.Enabled = false;
            this.tool_saveas.Image = ((System.Drawing.Image)(resources.GetObject("tool_saveas.Image")));
            this.tool_saveas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tool_saveas.Name = "tool_saveas";
            this.tool_saveas.Size = new System.Drawing.Size(23, 22);
            this.tool_saveas.Text = "作業データを別名で保存";
            this.tool_saveas.Click += new System.EventHandler(this.menu_saveas_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolzoomin
            // 
            this.toolzoomin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolzoomin.Enabled = false;
            this.toolzoomin.Image = ((System.Drawing.Image)(resources.GetObject("toolzoomin.Image")));
            this.toolzoomin.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolzoomin.Name = "toolzoomin";
            this.toolzoomin.Size = new System.Drawing.Size(23, 22);
            this.toolzoomin.Text = "拡大";
            this.toolzoomin.Click += new System.EventHandler(this.toolzoomin_Click);
            // 
            // toolzoomout
            // 
            this.toolzoomout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolzoomout.Enabled = false;
            this.toolzoomout.Image = ((System.Drawing.Image)(resources.GetObject("toolzoomout.Image")));
            this.toolzoomout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolzoomout.Name = "toolzoomout";
            this.toolzoomout.Size = new System.Drawing.Size(23, 22);
            this.toolzoomout.Text = "縮小";
            this.toolzoomout.Click += new System.EventHandler(this.toolzoomout_Click);
            // 
            // textBox1
            // 
            this.textBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.textBox1.Location = new System.Drawing.Point(215, 97);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(91, 23);
            this.textBox1.TabIndex = 10;
            this.textBox1.Visible = false;
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            this.textBox1.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textBox1_PreviewKeyDown);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // dialog_openold
            // 
            this.dialog_openold.FileName = "openFileDialog1";
            this.dialog_openold.Filter = "labelファイル|*.label";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // dialog_opennew
            // 
            this.dialog_opennew.Filter = "ラベルデータ|*lbl";
            // 
            // dialog_savenew
            // 
            this.dialog_savenew.Filter = "ラベルデータ|*.lbl";
            // 
            // dialog_savetxt
            // 
            this.dialog_savetxt.Filter = "テキスト|*.txt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(678, 293);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.pic);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Meiryo UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1980, 332);
            this.MinimumSize = new System.Drawing.Size(300, 332);
            this.Name = "Form1";
            this.Text = "うぇーぶかったー（あるふぁ2）";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.wavemenu.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menu_file;
        private System.Windows.Forms.ToolStripMenuItem menu_load;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem menu_open;
        private System.Windows.Forms.ToolStripMenuItem menu_save;
        private System.Windows.Forms.ToolStripMenuItem menu_saveas;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem menu_export;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem menu_end;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.OpenFileDialog dialog_load;
        private System.Windows.Forms.ToolStripMenuItem 表示VToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 波形モードToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu_wavemode1;
        private System.Windows.Forms.ToolStripMenuItem menu_wavemode2;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel stlabel1;
        private System.Windows.Forms.ToolStripStatusLabel stlabel2;
        private System.Windows.Forms.ContextMenuStrip wavemenu;
        private System.Windows.Forms.ToolStripMenuItem 全体表示ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 選択部分を上でカットToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 選択部分を下でカットToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tool_load;
        private System.Windows.Forms.ToolStripButton tool_open;
        private System.Windows.Forms.ToolStripButton tool_save;
        private System.Windows.Forms.ToolStripButton tool_saveas;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripButton toolzoomin;
        private System.Windows.Forms.ToolStripButton toolzoomout;
        private System.Windows.Forms.OpenFileDialog dialog_openold;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.OpenFileDialog dialog_opennew;
        private System.Windows.Forms.SaveFileDialog dialog_savenew;
        private System.Windows.Forms.ToolStripMenuItem 互換データToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu_oldlabel;
        private System.Windows.Forms.ToolStripMenuItem なめうぇーぶ互換ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menu_vflread;
        private System.Windows.Forms.ToolStripMenuItem menu_vflsave;
        private System.Windows.Forms.SaveFileDialog dialog_savetxt;
    }
}

