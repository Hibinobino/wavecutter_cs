﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace NewLabelData
{
    public class labeldata
    {
        public int index;
        public string name;
    }
    public class NewLabel
    {
        public List<labeldata>[] OpenNewData(string filename)
        {
            List<labeldata>[] list = new List<labeldata>[] { new List<labeldata>(), new List<labeldata>() };

            using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader st = new StreamReader(fs, Encoding.UTF8))
                {
                    Console.WriteLine(st.ReadLine());
                    Console.WriteLine(st.ReadLine());
                    for (int y = 0; y < 2; y++)
                    {
                        string s = st.ReadLine();
                        int count = Int32.Parse(s);
                        //Console.WriteLine("count: " + count);
                        for (int x = 0; x < count; x++)
                        {
                            labeldata a = new labeldata();
                            string[] split = st.ReadLine().Split(':');
                            a.index = int.Parse(split[0]);
                            a.name = split[1];
                            list[y].Add(a);
                            //Console.WriteLine(list[y][x].index + ":" + list[y][x].name);
                        }
                        //list[y].Sort((aa, b) => aa.index.CompareTo(b.index));
                    }
                }
            }
            return list;

        }

        public void SaveNewData(string filename, List<labeldata>[] list, string wavefile)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter st = new StreamWriter(fs, Encoding.UTF8))
                {
                    st.WriteLine("うぇーぶかったー Alpha-201904");
                    st.WriteLine(wavefile);
                    for (int y = 0; y < 2; y++)
                    {
                        st.WriteLine(list[y].Count);
                        for (int x = 0; x < list[y].Count; x++)
                        {
                            st.WriteLine(list[y][x].index + ":" + list[y][x].name);
                        }
                    }
                }
            }
        }

        public void SaveVFLtxtData(string filename, List<labeldata>[] list, string wavefile)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter st = new StreamWriter(fs, Encoding.UTF8))
                {
                    st.WriteLine("<leaves>");
                    for (int y = 0; y < 1; y++)
                    {
                        for (int x = 1; x < list[y].Count - 1; x++)
                        {
                            // <leaf text="て" time="20.354" tag="" />
                            int ms = (int)(list[y][x].index / 44.1);
                            st.WriteLine("<leaf text=\"" + list[y][x].name + "\" time=\"" + ((float)ms / (float)1000) + "\" tag=\"\" />");
                        }
                    }

                    st.WriteLine("</leaves>");
                }
            }
        }

    }
}
