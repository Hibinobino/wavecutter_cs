﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Drawing
{
    public class Draw
    {
        public void Label(Bitmap bmp, Color labelcolor)
        {
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.Transparent);
                Rectangle rect = new Rectangle(
                    2 + 2,
                    (bmp.Height - (int)(10 * 1.33) - 10) / 2,
                    100, (int)(10 * 1.33) + 10);
                LinearGradientBrush grad = new LinearGradientBrush(
                    rect,
                    labelcolor,
                    Color.Transparent,
                    LinearGradientMode.Horizontal);
                Pen pen = new Pen(labelcolor, 2);

                g.DrawLine(pen, 2, 0, 2, bmp.Height);

                g.FillRectangle(grad,//Brushes.Black,
                    2 + 2, (bmp.Height - ((int)(10 * 1.33) + 10)) / 2,
                    100, ((int)(10 * 1.33) + 10));

                grad.Dispose();
                pen.Dispose();
            }
        }
    }
}
