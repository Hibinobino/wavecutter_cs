﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OldLabelData
{
    public class OldLabel
    {

        public int[] label;
        public string[] labelname;

        public bool ReadOldLabel(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            BinaryReader bin = new BinaryReader(fs);

            try
            {
                Console.WriteLine("\r\n\r\nLine----------------------------------------------------");
                Console.WriteLine(Encoding.UTF8.GetString(bin.ReadBytes(4)) + " / ???：" + bin.ReadInt32()); //hspv
                int var_count = bin.ReadInt32();
                Console.WriteLine("保存された変数の数：" + var_count + " / ???：" + bin.ReadInt32()); //no of vars

                int[] vname_no = new int[var_count];
                int[] vsize = new int[var_count];
                int[] vname_index = new int[var_count];
                int[] varray_for = new int[var_count];
                string[] type = new string[var_count];
                for (int x = 0; x < var_count; x++)
                {
                    Console.WriteLine("");
                    Console.WriteLine("VAR_ID : " + x + "--------------------------");
                    vname_index[x] = bin.ReadInt32();
                    Console.WriteLine("文字数インデックス？：" + vname_index[x]);
                    vname_no[x] = bin.ReadInt32();
                    Console.WriteLine("文字数 + 1：" + vname_no[x] + " / ???：" + bin.ReadInt32() + " / ???：" + bin.ReadInt32());

                    Console.WriteLine("");
                    int buf = bin.ReadInt16();
                    if (buf == 4) type[x] = "数値";
                    if (buf == 2) type[x] = "文字列";
                    Console.WriteLine("種別：" + type[x] + " / ???：" + bin.ReadInt16() + " / ???：" + bin.ReadInt32());
                    varray_for[x] = bin.ReadInt32();


                    Console.WriteLine("要素数：" + varray_for[x] + " / ???：" + bin.ReadInt32());

                    Console.WriteLine("");
                    Console.WriteLine("???：" + bin.ReadInt32() + " / ???：" + bin.ReadInt32());
                    vsize[x] = bin.ReadInt32();
                    Console.WriteLine("変数サイズ：" + vsize[x] + " / ???：" + bin.ReadByte() + " / ???：" + bin.ReadByte() + " / ???：" + bin.ReadByte() + " / ???：" + bin.ReadByte());

                    Console.WriteLine("");
                    Console.WriteLine("???：" + bin.ReadByte() + " / ???：" + bin.ReadByte() + " / ???：" + bin.ReadByte() + " / ???：" + bin.ReadByte() + " / ???：" + bin.ReadInt32() + " / ???：" + bin.ReadInt32() + " / ???：" + bin.ReadInt32());
                }
                Console.WriteLine("");

                for (int x = 0; x < var_count; x++)
                {

                    //変数の名前！
                    string vname = Encoding.UTF8.GetString(bin.ReadBytes(vname_no[x] - vname_index[x] - 1));
                    Console.WriteLine(vname);
                    bin.ReadByte();

                    if (vname == "label") label = new int[varray_for[x]];
                    if (vname == "labelname") labelname = new string[varray_for[x]];

                    for (int y = 0; y < varray_for[x]; y++)
                    {
                        if (vname == "label")
                        {
                            label[y] = bin.ReadInt32();
                            //if (label[y] == 1) Console.WriteLine(y + ":" + label[y]);
                        }
                        else if (vname == "labelname")
                        {
                            bin.ReadBytes(4); //4
                            int byt = bin.ReadInt32(); //4

                            byte[] b = bin.ReadBytes(byt);
                            int c = 0;
                            while (b[c] != 0)
                            {
                                c += 1;
                            }
                            byte[] bb = new byte[c];
                            for (int i = 0; i < c; i++)
                            {
                                bb[i] = b[i];
                            }
                            labelname[y] = Encoding.GetEncoding("shift-jis").GetString(bb);
                            
                        }
                    }
                }
            }
            catch (Exception)
            {
                fs.Close();
                bin.Close();
                return false;

            }
            finally
            {
                fs.Close();
                bin.Close();
            }
            return true;
        }

    }
}
