﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using WaveFile;
using OldLabelData;
using Drawing;
using NewLabelData;

namespace WavecutVS
{
    public partial class Form1 : Form
    {

        string filename;
        string dataname;
        string exepath;
        short[] zoomMax;
        short[] zoomMin;
        Bitmap bitmap = new Bitmap(2000, 200);
        Bitmap labelbmp = new Bitmap(105, 100);
        bool openflg = false;
        bool wavehover = false;
        bool wavedrag = false;
        bool wavesel = false;
        bool changed = false;
        bool labelchange = false;
        Point m;
        int blk = 128;
        int sel1 = 0;
        int sel2 = 0;
        int selstart;
        int selend;
        int snap;
        string grabs;
        DealWave wave = new DealWave();
        int fx = -1;
        int fy = 0;
        string directory;
        string fname;
        OldLabel olddata = new OldLabel();
        NewLabel newdata = new NewLabel();
        Draw draw = new Draw();

        List<labeldata>[] indexlist = new List<labeldata>[2] { new List<labeldata>(), new List<labeldata>() };

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            hScrollBar1.Enabled = false;
            DrawWave();
            pic.MouseWheel += new System.Windows.Forms.MouseEventHandler(ScrollPic);

            exepath = Path.GetDirectoryName(Application.ExecutablePath);

            draw.Label(labelbmp, Color.Tomato);

        }

        //メニュー系
        private void menu_open_Click(object sender, EventArgs e)
        {
            textBox1.Visible = false;
            if (changed == true)
            {
                DialogResult result = MessageBox.Show(
                    "データは変更されています。保存しますか？",
                    "保存確認",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);

                if (result == DialogResult.Cancel)
                {
                    return;
                }
            }

            if (dialog_opennew.ShowDialog() == DialogResult.OK)
            {
                indexlist[0].Clear();
                indexlist[1].Clear();
                dataname = dialog_opennew.FileName;
                indexlist = newdata.OpenNewData(dataname);

                for (int x = 0; x < 2; x++)
                {
                    if (indexlist[x].Exists(n => n.index == wave.whd.D_Size / wave.whd.F_block - 1) == false)
                    {

                        AddList(x, wave.whd.D_Size / wave.whd.F_block - 1, "*");
                    }
                }
            }
            else
            {
                return;
            }

            DrawWave();
        }
        private void menu_save_Click(object sender, EventArgs e)
        {
            if (dataname == null)
            {
                menu_saveas_Click(sender, e);

            }
            else newdata.SaveNewData(dataname, indexlist, filename);

            ChangeState(false);
        }
        private void menu_vflsave_Click(object sender, EventArgs e)
        {
            dialog_savetxt.FileName = Path.GetFileNameWithoutExtension(filename) + ".txt";
            if (dialog_savetxt.ShowDialog() == DialogResult.OK)
            {
                dataname = dialog_savetxt.FileName;
            }
            else
            {
                return;
            }
            newdata.SaveVFLtxtData(dataname, indexlist, filename);
        }
        private void menu_saveas_Click(object sender, EventArgs e)
        {
            dialog_savenew.FileName = Path.GetFileNameWithoutExtension(filename) + ".lbl";
            if (dialog_savenew.ShowDialog() == DialogResult.OK)
            {
                dataname = dialog_savenew.FileName;
            }
            else
            {
                return;
            }
            newdata.SaveNewData(dataname, indexlist, filename);
            ChangeState(false);
        }
        private void menu_end_Click(object sender, EventArgs e)
        {
            if (changed == true)
            {
                DialogResult result = MessageBox.Show(
                    "データは変更されています。保存しますか？",
                    "保存確認",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);

                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    //保存処理
                }

            }
            this.Close();
        }
        private void menu_load_Click(object sender, EventArgs e)
        {
            textBox1.Visible = false;
            dataname = null;

            if (changed == true)
            {
                DialogResult result = MessageBox.Show(
                    "データは変更されています。保存しますか？",
                    "保存確認",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation);

                if (result == DialogResult.Cancel)
                {
                    return;
                }
                else if (result == DialogResult.Yes)
                {
                    //保存処理
                }

            }
            if (dialog_load.ShowDialog() == DialogResult.OK)
            {
                filename = dialog_load.FileName;

                openflg = wave.ReadWave(filename);
                if (openflg == false) return;
                Console.WriteLine("waveファイルを読み込みました");

                this.Text = "うぇーぶかったー - " + filename;
                directory = Path.GetDirectoryName(filename);
                fname = Path.GetFileNameWithoutExtension(filename);

                indexlist[0].Clear();
                indexlist[1].Clear();

                InitSummary(blk);
                Console.WriteLine("サマリー完了");
                hScrollBar1.Value = 0;
                hScrollBar1.Enabled = true;
                Console.WriteLine("スクロール準備完了");
                DrawWave();
                Console.WriteLine("WAVE範囲描画完了");

                sel1 = 0; sel2 = 0;
                Console.WriteLine("編集準備完了");


                AddList(0, 0, "*");
                AddList(1, 0, "*");
                AddList(0, wave.whd.D_Size - 1, "*");
                AddList(1, wave.whd.D_Size - 1, "*");

                menu_open.Enabled = true;
                menu_save.Enabled = true;
                menu_saveas.Enabled = true;
                menu_export.Enabled = true;
                menu_oldlabel.Enabled = true;
                tool_open.Enabled = true;
                tool_save.Enabled = true;
                tool_saveas.Enabled = true;
                toolzoomin.Enabled = true;
                toolzoomout.Enabled = true;
                menu_vflsave.Enabled = true;
            }
            DrawWave();
        }
        private void menu_export_Click(object sender, EventArgs e)
        {
            string foldername = fname + DateTime.Now.ToString("yyyyMMdd-HHmmss");
            string savedir = exepath + "\\" + foldername;

            Directory.CreateDirectory(savedir);

            Console.WriteLine(savedir);

            for (int x = 0; x < indexlist[0].Count - 1; x++)
            {
                wave.SaveWave(indexlist[0][x].index, indexlist[0][x + 1].index, savedir + "\\" + indexlist[0][x].name);
            }
            for (int x = 0; x < indexlist[1].Count - 1; x++)
            {
                wave.SaveWave(indexlist[1][x].index, indexlist[1][x + 1].index, savedir + "\\" + indexlist[1][x].name);
            }
            Console.WriteLine("SAVED");
        }
        private void menu_oldlabel_Click(object sender, EventArgs e)
        {
            if (dialog_openold.ShowDialog() == DialogResult.OK)
            {
                if (olddata.ReadOldLabel(dialog_openold.FileName) == false)
                {
                    MessageBox.Show(
                        "旧版の作業データではないか、作業データが破損しているため読み込めませんでした。",
                        "データ読み込みエラー",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    return;
                }

                textBox1.Visible = false;

                if (indexlist != null)
                {
                    indexlist[0].Clear();
                    indexlist[1].Clear();
                }

                for (int x = 0; x < olddata.labelname.Length; x++)
                {
                    if (olddata.label[x] == 1) AddList(0, x * 128, olddata.labelname[x]);
                }
                AddList(1, 0, "*");
                AddList(0, wave.whd.D_Size / wave.whd.F_block - 1, "*");
                AddList(1, wave.whd.D_Size / wave.whd.F_block - 1, "*");

                textBox1.Visible = false;
                DrawWave();
                ChangeState(true);
            }
        }
        private void menu_wavemode1_Click(object sender, EventArgs e)
        {
            menu_wavemode1.Checked = true;
            menu_wavemode2.Checked = false;

            DrawWave();
        }
        private void menu_wavemode2_Click(object sender, EventArgs e)
        {
            menu_wavemode1.Checked = false;
            menu_wavemode2.Checked = true;

            DrawWave();
        }

        //選択範囲を描画
        private void DrawWave()
        {
            int index = hScrollBar1.Value;

            Graphics g = Graphics.FromImage(bitmap);
            g.FillRectangle(Brushes.Pink, 0, 0, pic.Width, pic.Height);

            if (openflg == true)
            {
                DrawWaveForm(g, index);
                DrawRangeRect(g, index);

                DrawLabel(g, index);

                int my = m.Y / (pic.Height / 2) * (pic.Height / 2);
                if (wavehover == true) g.DrawLine(Pens.Red, m.X, my, m.X, my + pic.Height / 2);

            }
            g.Dispose();
            pic.Image = bitmap;
        }
        private void DrawRangeRect(Graphics g, int index)
        {
            if (wavesel == false) return;
            int my = m.Y / (pic.Height / 2) * (pic.Height / 2);
            selstart = Math.Min(sel1, sel2) - index;
            selend = Math.Max(sel1, sel2) - index;
            SolidBrush b = new SolidBrush(Color.FromArgb(100, Color.PaleVioletRed));
            g.FillRectangle(b, selstart, my, selend - selstart, pic.Height / 2);
            g.FillRectangle(b, selstart, 0, selend - selstart, pic.Height);
            b.Dispose();
        }

        private void DrawLabel(Graphics g, int index)
        {
            if (indexlist == null) return;

            Font font = new Font("Meiryo UI", 10);

            for (int y = 0; y < 2; y++)
            {
                if (indexlist[y] == null) continue;
                int first = indexlist[y].FindIndex(
                    n =>
                    n.index >= index * blk
                    );
                int last = indexlist[y].FindLastIndex(
                    n =>
                    n.index < (index + pic.Width) * blk
                    );
                if (first == -1) continue;
                int height = (int)(pt2px(10) + 10);
                for (int x = first - 1; x <= last + 1; x++)
                {
                    if (x < 0) continue;
                    if (x >= indexlist[y].Count) break;

                    g.DrawImageUnscaled(labelbmp, indexlist[y][x].index / blk - index - 2, y * (pic.Height / 2));

                    g.DrawString(indexlist[y][x].name,
                        font, Brushes.Black,
                        indexlist[y][x].index / blk - index + 4,
                        pic.Height / 2 * y + (pic.Height / 4 - (int)(font.Height * 1.33) / 2));

                }

            }
            if (labelchange == true)
            {
                int y = m.Y / (pic.Height / 2);

                g.DrawImageUnscaled(labelbmp, m.X + 2 - 2, y * (pic.Height / 2));

                g.DrawString(grabs,
                    font, Brushes.Black,
                    m.X + 4,
                    pic.Height / 2 * y + (pic.Height / 4 - (int)(font.Height * 1.33) / 2));
            }
            font.Dispose();
        }
        private void DrawWaveForm(Graphics g, int index)
        {
            Pen p2 = new Pen(Color.FromArgb(90, Color.Salmon), 4);
            if (menu_wavemode1.Checked == true)
            {
                for (int x = 0; x < pic.Width + 1; x++)
                {
                    if (x + index >= zoomMax.Length) break;
                    g.DrawLine(Pens.White,
                        x, pic.Height / 2 - zoomMax[x + index] * pic.Height / 65536,
                        x, pic.Height / 2 - zoomMin[x + index] * pic.Height / 65536
                        );
                }
                g.DrawLine(p2, 0, pic.Height / 2, pic.Width, pic.Height / 2);
            }
            else
            {
                Pen p = new Pen(Color.White, 2);

                for (int x = 0; x < pic.Width + 1; x++)
                {
                    if (x + index >= zoomMax.Length) break;
                    if ((x + index) % 3 != 0) continue;
                    int zmax = 0; int zmin = 0;
                    for (int y = 0; y < Math.Min(3, zoomMax.Length - index - x); y++)
                    {
                        zmax = Math.Max(zmax, zoomMax[x + index + y]);
                        zmin = Math.Min(zmin, zoomMin[x + index + y]);
                    }
                    g.DrawLine(p,
                        x, pic.Height / 2 - zmax * pic.Height / 65536,
                        x, pic.Height / 2 - zmin * pic.Height / 65536
                        );
                }
                g.DrawLine(p2, 0, pic.Height / 2, pic.Width, pic.Height / 2);
                p.Dispose();
            }
            p2.Dispose();


        }

        private void InitSummary(int block)
        {

            //int length = waveData.samplesL.Length; //もともとの要素数
            int length = wave.whd.data.Length;
            int summ = length / block; //荒くする
            if (length % block != 0) summ += 1;

            zoomMax = new short[summ];
            zoomMin = new short[summ];

            int index;

            for (int i = 0; i < summ; i++)
            {
                short max = 0;
                short min = 0;
                for (int j = 0; j < block; j++)
                {
                    index = i * block + j;
                    if (index >= length) break;
                    max = Math.Max(max, (short)wave.whd.data[index]);
                    min = Math.Min(min, (short)wave.whd.data[index]);
                }
                zoomMax[i] = max;
                zoomMin[i] = min;
            }
            //荒い解像度でスクロール指定
            hScrollBar1.SmallChange = pic.Width / 4;
            hScrollBar1.LargeChange = summ / 10;
            hScrollBar1.Maximum = summ + summ / 10 - pic.Width;
        }


        private void Form1_Resize(object sender, EventArgs e)
        {
            if (openflg == false) return;
            int summ = zoomMax.Length;
            hScrollBar1.Maximum = summ + summ / 10 - pic.Width;
            if (hScrollBar1.Value > summ - pic.Width)
            {
                hScrollBar1.Value = summ - pic.Width - (hScrollBar1.Value - summ + pic.Width);
            }
            DrawWave();
        }

        private void pic_MouseEnter(object sender, EventArgs e)
        {
            if (openflg == false) return;
            wavehover = true;
            DrawWave();
        }

        private void pic_MouseMove(object sender, MouseEventArgs e)
        {
            if (openflg == false) return;
            m.Y = e.Y;
            m.X = SnapCheck(e.X);
            int index = hScrollBar1.Value;

            //選択範囲ドラッグ中の場合
            if (wavedrag == true)
            {
                sel2 = m.X + index;
                textBox1.Visible = false;
            }
            else
            {
                if (pic.Cursor == Cursors.VSplit)
                {
                    //ラベル位置変更の時
                }
            }

            DrawWave();
        }

        private int SnapCheck(int mousex)
        {
            int index;
            int index2;
            int y = m.Y / (pic.Height / 2);

            if (mousex < 0) mousex = 0;

            //右方向に一番近い
            index = indexlist[y].FindLastIndex(n => n.index <= (mousex + hScrollBar1.Value) * blk);

            //左方向に一番近い
            index2 = indexlist[y].FindIndex(n => n.index > (mousex + hScrollBar1.Value) * blk);

            if (index != -1)
            {
                if (Math.Abs((indexlist[y][index].index / blk - hScrollBar1.Value) - mousex) <= 5)
                {
                    if (pic.Cursor != Cursors.VSplit) pic.Cursor = Cursors.VSplit;
                    snap = index;
                    return ((int)indexlist[y][index].index / blk - hScrollBar1.Value);
                }
            }
            else if (index2 != -1)
            {
                if (Math.Abs((indexlist[y][index2].index / blk - hScrollBar1.Value) - mousex) <= 5)
                {
                    if (pic.Cursor != Cursors.VSplit) pic.Cursor = Cursors.VSplit;
                    snap = index2;
                    return ((int)indexlist[y][index2].index / blk - hScrollBar1.Value);
                }
            }
            if (pic.Cursor != Cursors.Default) pic.Cursor = Cursors.Default;
            return mousex;
        }

        private void pic_MouseLeave(object sender, EventArgs e)
        {
            if (openflg == false) return;
            wavehover = false;
            DrawWave();
        }

        private void pic_MouseDown(object sender, MouseEventArgs e)
        {
            if (openflg == false) return;
            int index = hScrollBar1.Value;
            if (e.Button == MouseButtons.Left)
            {

                if (pic.Cursor != Cursors.VSplit)
                {
                    //選択範囲
                    wavesel = true;
                    wavedrag = true;
                    sel1 = m.X + index;
                }
                else
                {
                    //ラベル位置調整
                    if (wavesel == true) wavesel = false;
                    labelchange = true;
                    if (textBox1.Visible == true) textBox1.Visible = false;
                    grabs = indexlist[m.Y / (pic.Height / 2)][snap].name;
                    AddList(m.Y / (pic.Height / 2), (m.X + hScrollBar1.Value) * blk, "*");
                    ChangeState(true);

                }
            }

        }

        private void pic_MouseUp(object sender, MouseEventArgs e)
        {
            if (openflg == false) return;
            wavedrag = false;
            int index = hScrollBar1.Value;

            if (e.Button == MouseButtons.Left)
            {
                if (sel1 == e.X + index) wavesel = false;

                //範囲選択してなければ歌詞変更
                if (wavesel == false)
                {
                    if (labelchange == true)
                    {
                        AddList(m.Y / (pic.Height / 2), (m.X + hScrollBar1.Value) * blk, grabs);
                        int i = indexlist[m.Y / (pic.Height / 2)].FindIndex(n => n.index == (m.X + hScrollBar1.Value) * blk);
                        labelchange = false;
                        ChangeState(true);
                    }
                    else if (textBox1.Visible == true)
                    {
                        if (indexlist[fy][fx].name != textBox1.Text)
                        {
                            indexlist[fy][fx].name = textBox1.Text;
                            ChangeState(true);
                        }
                    }
                    fy = m.Y / (pic.Height / 2);
                    fx = indexlist[fy].FindLastIndex(n => n.index / blk <= m.X + hScrollBar1.Value);
                    ShowLabelTextBox();
                    textBox1.Visible = true;
                    wavesel = false;
                }
            }
            else if (e.Button == MouseButtons.Right)
            {
                int a = AddList(e.Y / (pic.Height / 2), (m.X + index) * blk, "*");
                ChangeState(true);
                if (a == -1)
                {
                    textBox1.Visible = false;
                }
                else
                {
                    if (textBox1.Visible == true)
                    {
                        indexlist[fy][fx].name = textBox1.Text;
                    }
                    fy = m.Y / (pic.Height / 2);
                    fx = indexlist[fy].FindLastIndex(n => n.index / blk <= m.X + hScrollBar1.Value);
                    ShowLabelTextBox();
                    textBox1.Visible = true;
                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            for (int x = 0; x < indexlist[0].Count; x++)
            {
                Console.WriteLine("" + indexlist[0][x].index);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {


        }

        private void ChangeState(bool isitchanged)
        {
            if (changed != isitchanged)
            {
                changed = isitchanged;
                if (changed == true) this.Text += "*";
                if (changed == false) this.Text = "うぇーぶかったー - " + filename;
            }
        }
        private int AddList(int y, int sample, string name)
        {
            labeldata a = new labeldata();
            long exist = indexlist[y].FindIndex(n => n.index >= sample && n.index - sample < blk);

            if (exist == -1)
            {
                a.index = sample; a.name = name;
                indexlist[y].Add(a);
                Console.WriteLine(sample + "を追加しました");
                indexlist[y].Sort((aa, b) => aa.index.CompareTo(b.index));
                if (textBox1.Visible == true)
                {
                    indexlist[fy][fx].name = textBox1.Text;
                }
                fy = m.Y / (pic.Height / 2);
                fx = indexlist[fy].FindLastIndex(n => n.index / blk <= m.X + hScrollBar1.Value);

                //ShowLabelTextBox();
                return 0;
            }
            else
            {
                indexlist[y].Remove(indexlist[y][(int)exist]);
                Console.WriteLine(sample + "を削除しました");
                indexlist[y].Sort((aa, b) => aa.index.CompareTo(b.index));
                return -1;
            }

        }

        private void pic_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (openflg == false) return;
            int index = hScrollBar1.Value;
            int current = (e.X + index) * blk;

            int row = e.Y / (pic.Height / 2);

            int first = indexlist[row].FindLastIndex(n => n.index <= current);
            Console.WriteLine(first + "を発見");

            int from = Math.Max(index * blk, indexlist[row][first].index);
            int to = Math.Min((index + pic.Width) * blk, indexlist[row][first + 1].index);

            Console.WriteLine(row + "列の" + from + "～" + to + "を再生します");
            wave.PlayWave(from, to);
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            //label2.Text = "(" + e.X + "," + e.Y + ")";
        }

        private void ScrollPic(object sender, MouseEventArgs e)
        {
            int amount = hScrollBar1.SmallChange / 2;
            if (e.Delta < 0)
            {
                hScrollBar1.Value = Math.Max(hScrollBar1.Value - amount, 0);
            }
            else if (e.Delta > 0)
            {
                hScrollBar1.Value = Math.Min(hScrollBar1.Value + amount, hScrollBar1.Maximum);
            }
            DrawWave();
            if (textBox1.Visible == true) MoveLabelTextBox();
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            DrawWave();
            if (textBox1.Visible == true) MoveLabelTextBox();
        }

        private void MoveLabelTextBox()
        {
            long indexpx = indexlist[fy][fx].index / blk;
            textBox1.Left = (int)indexpx - hScrollBar1.Value + 2;
            textBox1.Top = (int)(pic.Height / 2 * fy + (pic.Height / 2 - pt2px(12) - 10) / 2) + 49;
            Console.WriteLine("movelabeltext" + (fx) + "/" + textBox1.Left);
            if (textBox1.Left > pic.Width) textBox1.Left = pic.Width;
            if (textBox1.Left < -textBox1.Width) textBox1.Left = -textBox1.Width;
        }

        private int pt2px(int pointsize)
        {
            int pixelsize = (int)(pointsize * 1.33);
            return pixelsize;
        }

        private void textBox1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Tab:
                    //すでに表示されていた場合は変更を反映する
                    if (textBox1.Visible == true)
                    {
                        indexlist[fy][fx].name = textBox1.Text;
                    }
                    //shiftキー有無で方向を変える
                    if ((e.KeyData & Keys.Shift) == Keys.Shift)
                    {
                        //shiftといっしょ：遡る
                        if (fx > 0) fx -= 1;
                        if (indexlist[fy][fx].index / blk - hScrollBar1.Value < 0)
                        {
                            hScrollBar1.Value = (int)indexlist[fy][fx].index / blk;
                        }
                    }
                    else if (e.KeyData == Keys.Tab)
                    {
                        if (fx < indexlist[fy].Count - 2) fx += 1;
                        if (indexlist[fy][fx].index / blk - hScrollBar1.Value > pic.Width - 200)
                        {
                            hScrollBar1.Value = (int)indexlist[fy][fx].index / blk - pic.Width + 200;
                        }
                    }
                    DrawWave();
                    wave.PlayWave(indexlist[fy][fx].index, Math.Min(indexlist[fy][fx + 1].index, (hScrollBar1.Value + pic.Width) * blk));
                    ShowLabelTextBox();
                    break;
                case Keys.Space:
                    if (textBox1.Visible == true) break;
                    if (wavesel == true)
                    {
                        wave.PlayWave((selstart + hScrollBar1.Value) * blk, (selend + hScrollBar1.Value) * blk);
                    }
                    break;

            }
        }

        private void ShowLabelTextBox()
        {
            //新しいラベルに移動する！
            //フォーカス変更


            textBox1.Visible = false;
            MoveLabelTextBox();
            textBox1.Visible = true;
            textBox1.Text = indexlist[fy][fx].name;
            textBox1.Focus();
            textBox1.SelectAll();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)Keys.Enter:
                    if (textBox1.Visible == true)
                    {
                        indexlist[fy][fx].name = textBox1.Text;
                        textBox1.Visible = false;
                    }
                    e.Handled = true;
                    break;
            }
        }

        private void pic_Click(object sender, EventArgs e)
        {

        }

        private void toolzoomin_Click(object sender, EventArgs e)
        {
            if (blk > 16)
            {
                blk /= 2;
                sel1 *= 2;
                sel2 *= 2;
                hScrollBar1.Value *= 2;
            }
            InitSummary(blk);
            DrawWave();
        }

        private void toolzoomout_Click(object sender, EventArgs e)
        {
            if (blk < 4096)
            {
                blk *= 2;
                sel1 /= 2;
                sel2 /= 2;
                hScrollBar1.Value /= 2;
            }
            InitSummary(blk);
            DrawWave();
        }

        private void pic_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (wavesel == false) break;
                    Console.WriteLine("かっと");
                    AddList(m.Y / (pic.Height / 2), (selstart + hScrollBar1.Value) * blk, "*");
                    AddList(m.Y / (pic.Height / 2), (selend + hScrollBar1.Value) * blk, "*");
                    ChangeState(true);
                    break;
            }
            DrawWave();
        }

        
    }


}
